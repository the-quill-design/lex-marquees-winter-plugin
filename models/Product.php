<?php namespace Monologophobia\LexMarquees\Models;

use Model;

class Product extends Model {

    use \October\Rain\Database\Traits\Validation;
    use \Winter\Storm\Database\Traits\Sluggable;
    use \Winter\Storm\Database\Traits\Nullable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'monologophobia_lexmarquees_products';

    protected $slugs    = ['slug' => 'name'];
    protected $jsonable = ['points', 'extras', 'specification'];
    protected $nullable = ['extras'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required|string',
        'description' => 'required|string',
        'short_description' => 'required|string',
        'points' => 'required',
    ];

    public $belongsTo = [
        'category' => 'Monologophobia\LexMarquees\Models\Category'
    ];

    public $belongsToMany = [
        'occassions' => [
            'Monologophobia\LexMarquees\Models\Occassion',
            'table' => 'monologophobia_lexmarquees_product_occassion',
        ]
    ];

    public $attachOne = [
        'header' => ['System\Models\File', 'delete' => true],
        'specification_image' => ['System\Models\File', 'delete' => true],
    ];

    public $attachMany = [
        'images' => ['System\Models\File', 'delete' => true]
    ];

}
