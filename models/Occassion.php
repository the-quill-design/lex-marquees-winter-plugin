<?php namespace Monologophobia\LexMarquees\Models;

use Model;

class Occassion extends Model {

    use \October\Rain\Database\Traits\Validation;
    use \Winter\Storm\Database\Traits\Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'monologophobia_lexmarquees_occassions';

    protected $slugs = ['slug' => 'name'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required|string',
        'description' => 'required|string',
        'short_description' => 'required|string',
    ];

    public $belongsToMany = [
        'products' => [
            'Monologophobia\LexMarquees\Models\Product',
            'table' => 'monologophobia_lexmarquees_product_occassion',
        ]
    ];

    public $attachOne = [
        'header' => ['System\Models\File', 'delete' => true]
    ];

}
