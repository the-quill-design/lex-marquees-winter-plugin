<?php namespace Monologophobia\LexMarquees\Models;

use Model;

class Category extends Model {

    use \October\Rain\Database\Traits\Validation;
    use \Winter\Storm\Database\Traits\Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'monologophobia_lexmarquees_categories';

    protected $slugs = ['slug' => 'name'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required|string',
        'description' => 'required|string',
        'short_description' => 'required|string',
    ];

    public $hasMany = [
        'products' => ['Monologophobia\LexMarquees\Models\Product', 'delete' => true]
    ];

    public $attachOne = [
        'header' => ['System\Models\File', 'delete' => true]
    ];

}
