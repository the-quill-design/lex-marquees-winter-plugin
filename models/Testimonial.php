<?php namespace Monologophobia\LexMarquees\Models;

use Model;

class Testimonial extends Model {

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'monologophobia_lexmarquees_testimonials';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'content'  => 'required|string',
        'author'   => 'required|string',
        'location' => 'required|string',
    ];

}
