<?php namespace Monologophobia\LexMarquees;

use Lang;
use Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase {

    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function registerNavigation() {
        return [
            'lexmarquees' => [
                'label' => "Marquees",
                'url'   => Backend::url('monologophobia/lexmarquees/products'),
                'icon'  => "icon-home",
                'order' => 601,
                'sideMenu' => [
                    'products' => [
                        'label' => "Products",
                        'url'   => Backend::url('monologophobia/lexmarquees/products'),
                        'icon'  => "icon-home",
                    ],
                    'occassions' => [
                        'label' => 'Occassions',
                        'url'   => Backend::url('monologophobia/lexmarquees/occassions'),
                        'icon'  => 'icon-birthday-cake',
                    ],
                    'categories' => [
                        'label' => 'Categories',
                        'url'   => Backend::url('monologophobia/lexmarquees/categories'),
                        'icon'  => 'icon-list',
                    ],
                    'testimonials' => [
                        'label' => 'Testimonials',
                        'url'   => Backend::url('monologophobia/lexmarquees/testimonials'),
                        'icon'  => 'icon-users',
                    ],
                ]
            ]
        ];
    }

}
