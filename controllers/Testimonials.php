<?php namespace Monologophobia\LexMarquees\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

class Testimonials extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $bodyClass  = 'compact-container';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.LexMarquees', 'lexmarquees', 'testimonials');
    }

}
