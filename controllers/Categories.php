<?php namespace Monologophobia\LexMarquees\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

class Categories extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.RelationController',
    ];

    public $listConfig     = 'config_list.yaml';
    public $formConfig     = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $bodyClass  = 'compact-container';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.LexMarquees', 'lexmarquees', 'categories');
    }

}
