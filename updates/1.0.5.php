<?php namespace Monologophobia\LexMarquees\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointZeroPointFive extends Migration {
    
    public function up() {

        Schema::create('monologophobia_lexmarquees_testimonials', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('author');
            $table->string('location');
            $table->text('content');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });

    }
    
    public function down() {

        Schema::dropIfExists('monologophobia_lexmarquees_testimonials');

    }

}
