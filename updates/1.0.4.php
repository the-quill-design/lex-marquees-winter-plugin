<?php namespace Monologophobia\LexMarquees\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointZeroPointFour extends Migration {
    
    public function up() {

        Schema::table('monologophobia_lexmarquees_categories', function($table) {
            $table->string('slug');
            $table->string('short_description');
        });

        Schema::table('monologophobia_lexmarquees_occassions', function($table) {
            $table->string('slug');
            $table->string('short_description');
        });

        Schema::create('monologophobia_lexmarquees_products', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('monologophobia_lexmarquees_categories')->onDelete('cascade');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('name');
            $table->string('slug');
            $table->text('description');
            $table->string('short_description');
            $table->json('points');
            $table->json('extras')->nullable();
            $table->json('specification');
        });

        Schema::create('monologophobia_lexmarquees_product_occassion', function($table) {
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('monologophobia_lexmarquees_products')->onDelete('cascade');
            $table->integer('occassion_id')->unsigned();
            $table->foreign('occassion_id')->references('id')->on('monologophobia_lexmarquees_occassions')->onDelete('cascade');
        });

    }
    
    public function down() {

        Schema::dropIfExists('monologophobia_lexmarquees_products');

        Schema::table('monologophobia_lexmarquees_products', function($table) {
            $table->dropColumn('slug');
            $table->dropColumn('short_description');
        });

        Schema::table('monologophobia_lexmarquees_products', function($table) {
            $table->dropColumn('slug');
            $table->dropColumn('short_description');
        });

    }

}
